#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{
    ofstream myFile;
    cout << "Hellow world! Writing to " << argv[1] << endl;
    myFile.open(argv[1]);
    myFile << argv[2] << endl;
    myFile.close();
    return 0;
}
